import React from 'react'
import './Popular.css'

export default function Popular(props) {

   
  return (
    <>
   <div className="popular-container">
    <div className="heading">

   <h1>Popular Blogs</h1>
    </div>
   <div className="blog">
    <div className="blog-content">
    <div className="title">
         <label htmlFor="title">title</label>
        <input type="text" value='Popular Blog' disabled />
        </div>
        <div className="description">
        <label htmlFor="description">Description</label>
        <input type="text" value='This is  a popular blog.' disabled />
        <button onClick={() => props.handleDelete} id='deletebtn'>Delete</button>
        </div>
        </div>
    {
      props.Record.filter ((rec,idx) => rec.type === 'POPULAR').map((record,index)=>
      

  
        <div key={index} className="blog-content" >
        
        <div className="title">
         <label htmlFor="title">title</label>
        <input type="text" value={record.title} disabled />
        </div>
        <div className="description">
        <label htmlFor="description">Description</label>
        <input type="text" value={record.description} disabled />
        <button onClick={() => props.handleDelete(record,index)} id='deletebtn'>Delete</button>
        </div>
      </div>
      )}
      </div>
      </div>
      </>
)}
