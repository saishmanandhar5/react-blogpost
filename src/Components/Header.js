import React from 'react'
import Logo from './dotblog.png'

import './Header.css'
import './Navbar.css'
import './addblog.css'

import { useState } from 'react'
import Popular from './Popular'
import Featured from './Featured'



export default function Header() {
  const [modal, setModal] = useState(false)
  const addBlog = () => {
    setModal(!modal);
  }
  const [Record,setRecord]=useState([]);
  const [data,setData]=useState({
    idx:'',
    title:'',
    description:'',
    type:''
  });

  const handleInput=(e)=>{
    const idx= Record.length+1
    const name= e.target.name
    const value=e.target.value
    console.log(name, value);
    setData({...data,idx,[name]:value})
      
  }
  const handleDelete=(record,index)=>{
    console.log('in delete', index)
    Record.splice(index,1)
    setRecord([...Record]);
  }

  const handleSubmit=(e)=>{
    e.preventDefault();
    const newData={...data};
    setRecord([...Record, newData]);
     addBlog();
     setData({title:'',description:''})
  }
  return (
  
    <>
    {console.log(Record)}
      <div className="Navbar" style={{ display: 'flex', justifyContent: 'space-around', alignItems: 'center' }}>
        <img src={Logo} alt="logo" style={{ height: '50px', width: '130px' }} />
  
        <ul style={{ display: 'flex', justifyContent: 'center' }}>
          
          <li onClick={addBlog}>Add Blog</li>
        </ul>
      </div>
      {
        modal && (
  
          <div className="overlay" >
            <div className="container "  >
        <h1>Add Blog</h1>
        <form >
        <div className="txt-field">
  
          <label htmlFor="Title">Title</label>
          <input type="data" autoComplete='off' 
          onChange={handleInput}
          value={data.title}
          name='title' id='title-input' required />
          
        </div>
        <div className="txt-field">
  
          <label htmlFor="description">Description</label>
          <input type="data" autoComplete='off' 
          onChange={handleInput}
          value={data.description}
          name='description' id='description-input' />
  
        </div>
        <div className="ddl">
  
          <label htmlFor="Type">choose type</label>
          <select onChange={handleInput} name="type" id="dropdownlist" value={data.type}>
          <option value="">Select Type</option>
            <option value="POPULAR">Popular</option>
            <option value="FEATURED">Featured</option>
          </select>
  
        </div>
  
          <div className="addbtn">
  
            <button id='button' onClick={handleSubmit}>Add Blog</button>
            <button onClick={addBlog} id='button'>Cancel</button>
          </div>
          </form>
      </div>
  
          </div>
        )
      }
      <div className="blog-type">
  
      <div className="popular">
        <Popular Record={Record} handleDelete={handleDelete}/>
      </div>
      <div className="featured">
        <Featured Record={Record} handleDelete={handleDelete}/>
      </div>
      </div>
    </>
        
  
  )
  }
    
  
  

 


  
    
  
